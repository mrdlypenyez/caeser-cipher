﻿using System;
using System.Text;

namespace CeaserAlgorithmFile
{
    public class CeaserAlgorithm
    {

        public string Encryption(int key,string text)
        {
            string result = null;
            //byte[] ascii = Encoding.ASCII.GetBytes(text);
            int temp;
            
            foreach(char c in text)
            {
                
                temp = (int) c + key;
                result += (char) temp;
            }

            return result;
        }
        public string Descryption(int key,string text)
        {
            string result = null; 
            int temp;
            foreach(char c in text)
            {
                temp = (int) c - key;
                result += (char) temp;
            }
            return result;
        }
    }
}