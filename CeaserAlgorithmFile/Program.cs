﻿using System;
using System.IO;

namespace CeaserAlgorithmFile
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hangi İşlemi Yapmak İstersiniz: \n 1-Dosya İçeriği Şifrele \n 2-Şifreyi Aç");
            int control = Convert.ToInt32(Console.ReadLine());
            CeaserAlgorithm ceaserAlgorithm = new CeaserAlgorithm();
            string text, result;
            int key = 0;
            Console.WriteLine("Dosya Dizini giriniz: ");
            string path = Console.ReadLine();
            
            Console.WriteLine("Anahtar giriniz: ");
            key = Convert.ToInt32(Console.ReadLine());
            
            text = File.ReadAllText(path);
            if (control == 1)
            {
                result=ceaserAlgorithm.Encryption(key, text); 
                Console.WriteLine("Şifreli veriniz: "+result);
                Console.WriteLine("Kaydetmek istediğiniz dosya yolunu giriniz: "); 
                string newPath = Console.ReadLine();
                CreateFile(newPath,result);
            }
            else if (control == 2)
            {
                result = ceaserAlgorithm.Descryption(key, text);
                Console.WriteLine("Açılan Şifreli Veriniz: "+result);
                Console.WriteLine("Kaydetmek istediğiniz dosya yolunu giriniz: "); 
                string newPath = Console.ReadLine();
                CreateFile(newPath,result);
            }
            

            Console.ReadKey();
        }

        public static void CreateFile(string newPath, string result)
        {
            if (!File.Exists(newPath))
            {
                //File.Create(newPath);
                using (StreamWriter sw = File.CreateText(newPath))
                {
                    sw.WriteLine(result);
                    Console.WriteLine("Başarıyla Kaydedildi.");
                    sw.Close();
                }
            }
            else
            { 
                File.Delete(newPath);
                CreateFile(newPath,result);
            }
        }
    }
}